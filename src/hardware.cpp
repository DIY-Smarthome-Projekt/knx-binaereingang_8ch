#include <cstdint>
#include <stdio.h>
#include <stdarg.h>
#include <Arduino.h>
#include "hardware.h"
#include "KnxHelper.h"



void ledInfo(bool iOn)
{
#ifdef LED_YELLOW_PIN
    digitalWrite(LED_YELLOW_PIN, INFO_LED_PIN_ACTIVE_ON == iOn);
#endif
}

void ledProg(bool iOn)
{
#ifdef PROG_LED_PIN
    digitalWrite(PROG_LED_PIN, PROG_LED_PIN_ACTIVE_ON == iOn);
#endif
}

bool initUart() {
    Serial1.end();
    Serial1.begin(19200, SERIAL_8E1);
    for (uint16_t lCount = 0; !Serial1 && lCount < 1000; lCount++);
    if (!Serial1) {
        SERIAL_PORT.println("initUart() falied, something is going completely wrong!");
        return false;
    }
    return true;
}




void savePower()
{
    initUart();
    // SERIAL_PORT.println("savePower: Stop UART KNX communication...");
    sendUartCommand("STOP_MODE", U_STOP_MODE_REQ, U_STOP_MODE_IND);
     SERIAL_PORT.println("Switching off 5V rail");
    // turn off 5V rail (CO2-Sensor, Buzzer, RGB-LED-Driver, 1-Wire-Busmaster)
    uint8_t lBuffer[] = {U_INT_REG_WR_REQ_ACR0, ACR0_FLAG_XCLKEN | ACR0_FLAG_V20VCLIMIT };
    // get rid of knx reference
    Serial1.write(lBuffer, 2);
    // Turn off on-board leds
    sendUartCommand("READ_ACR0 (Analog control register 0)", U_INT_REG_RD_REQ_ACR0, ACR0_FLAG_XCLKEN | ACR0_FLAG_V20VCLIMIT);
    ledProg(false);
    ledInfo(false);
}

void restorePower(){
     SERIAL_PORT.println("Switching on 5V rail");
    // turn on 5V rail (CO2-Sensor & Buzzer)
    uint8_t lBuffer[] = {U_INT_REG_WR_REQ_ACR0, ACR0_FLAG_DC2EN | ACR0_FLAG_V20VEN | ACR0_FLAG_XCLKEN | ACR0_FLAG_V20VCLIMIT};
    initUart();
    Serial1.write(lBuffer, 2);
    // give all sensors some time to init
    delay(100);
    // SERIAL_PORT.println("restorePower: Start UART KNX communication...");
    sendUartCommand("EXIT_STOP_MODE", U_EXIT_STOP_MODE_REQ, U_RESET_IND);
}


uint8_t sendUartCommand(const char *iInfo, uint8_t iCmd, uint8_t iResp, uint8_t iLen /* = 0 */)
{
    // SERIAL_PORT.println("    Send command %s (%02X)... ", iInfo, iCmd);
    // send system state command and interpret answer
    Serial1.write(iCmd);

    int lResp = 0;
    uint32_t lUartResponseDelay = millis();
    while (!delayCheck(lUartResponseDelay, 100))
    {
        lResp = Serial1.read();
        if (lResp == iResp)
        {
            // SERIAL_PORT.println("OK - recieved expected response (%02X)", lResp);
            if (iLen == 1)
                lResp = Serial1.read();
            break;
        }
    }
    return lResp;
}

