#pragma once

#include <knx.h>
#include "KnxHelper.h"
#include "hardware.h"
#include "Binaereingang8CH.h"
//#include "PCF8575.h"

//#include "BinaereingangDevice.h"
#include "classReadInput.h"

classReadInput Inputs[MAX_NUMBER_OF_Channels];

PCF8575 pcf8575_IO(i2cAddr_IO);
PCF8575 pcf8575_VCC(i2cAddr_VCC);
PCF8575 pcf8575_LED(i2cAddr_LED);

uint32_t ReadInputDelay = 0;
//bool isDelay = true;
bool isReady = true;

uint32_t sendCyclicalDelay[MAX_NUMBER_OF_Channels] = {0};
uint8_t CH = 0;

//_____ State maschine EINGAENGE ______
enum State
{
  SetVCC = 1,
  ReadIO = 2,
  ResetVCC = 3,
  SendKO = 4,
  SetLEDS = 5,
  Finish = 10,
} CHState;
//______________________________________

uint8_t channel = 0;
uint8_t function = 1;

bool status_Led_ON = false;

bool state[MAX_NUMBER_OF_Channels] = {1};
bool state_Last[MAX_NUMBER_OF_Channels] = {1};
bool state_KO[MAX_NUMBER_OF_Channels];
bool state_LED[MAX_NUMBER_OF_Channels];
bool state_Lock[MAX_NUMBER_OF_Channels] = {0};
bool bus_start_sent[MAX_NUMBER_OF_Channels] = {0};
uint16_t state_LED_Out = 0xFFFF;

//uint16_t state_VCC[MAX_NUMBER_OF_Channels] = {65534, 65534, 65531, 65531, 65519, 65519, 65471, 65471};
uint8_t state_VCC[MAX_NUMBER_OF_Channels] = {8, 8, 10, 10, 12, 12, 14, 14};

void initInputs()
{
  for (int i = 0; i < MAX_NUMBER_OF_Channels; i++)
  {
    pcf8575_LED.pinMode(i, OUTPUT);
    pcf8575_LED.pcf8575_WriteALL(LOW);
  }

  uint16_t test = pcf8575_IO.pcf8575_ReadAll();
  SERIAL_PORT.println(test, BIN);

  for (int i = 0; i < MAX_NUMBER_OF_Channels; i++)
  {
    // read first time all inputs
    state[i] = pcf8575_IO.pcf8575_Read_NEU(i);
    state_Last[i] = state[i];
    SERIAL_PORT.print("State_");
    SERIAL_PORT.print(i + 1);
    SERIAL_PORT.print(": ");
    SERIAL_PORT.println(state[i]);

    if (knx.paramByte(getPar(BIN_CHOutputValue, i)))
      state_KO[i] = state[i]; // Wert für 'offen' = 1
    else
      state_KO[i] = !state[i]; // Wert für 'offen' = 0
  }
  // clear all
  pcf8575_LED.pcf8575_Clear();
}

void load_ETS_par()
{
  for (int i = 0; i < MAX_NUMBER_OF_Channels; i++)
  {
    bus_start_sent[i] = knx.paramByte(getPar(BIN_CHBuswiederkehrAbfrage, i));
  }
}

void set_State_LED(uint8_t channel)
{
  uint8_t function = 0;
  uint16_t state_old = state_LED_Out;

  if (knx.paramByte(BIN_LEDdisplayDirection) == 0) // LED leuchtet wenn Input geschlossen
  {
    state_LED[channel] = !state[channel];
#ifdef Binaer_8CH
    state_LED_Out ^= (-state_LED[7 - channel] ^ state_LED_Out) & (1 << channel + 8);
#endif
#ifdef Binaer_16CH
    state_LED_Out ^= (-state_LED[15 - channel] ^ state_LED_Out) & (1 << channel);
#endif
  }
  else
  {
    state_LED[channel] = state[channel];
#ifdef Binaer_8CH
    state_LED_Out ^= (-state_LED[7 - channel] ^ state_LED_Out) & (1 << channel + 8);
#endif
#ifdef Binaer_16CH
    state_LED_Out ^= (-state_LED[15 - channel] ^ state_LED_Out) & (1 << channel);
#endif
  }
}

void setLED_ON()
{
  pcf8575_LED.pcf8575_WriteALL(state_LED_Out);
  status_Led_ON = true;
}

void setLED_OFF()
{
  pcf8575_LED.pcf8575_WriteALL(0xFFFF);
  status_Led_ON = false;
}

void set_state_Lock(uint8_t channel)
{
  state_Lock[channel] = true;
}

void reset_state_Lock(uint8_t channel)
{
  state_Lock[channel] = false;
}

void isChange(uint8_t i)
{
  if (state[i] != state_Last[i] || bus_start_sent[i] == 1)
  {
    bus_start_sent[i] = 0; // Buswiederkehr Wert senden

    if (state_Lock[i] == 0)
    {
      if (knx.paramByte(getPar(BIN_CHOutputValue, i)))
        state_KO[i] = state[i]; // Wert für 'offen' = 1
      else
        state_KO[i] = !state[i]; // Wert für 'offen' = 0

      SERIAL_PORT.print("Kanal_");
      SERIAL_PORT.print(i + 1);
      SERIAL_PORT.print(": ");
      SERIAL_PORT.println(state_KO[i]);

      if (knx.paramByte(getPar(BIN_CHFallWertSenden, i)) && state_KO[i] == LOW || knx.paramByte(getPar(BIN_CHSteigWertSenden, i)) && state_KO[i] == HIGH)
      {
        knx.getGroupObject(getCom(BIN_KoGO_STATE_, i)).value(state_KO[i], getDPT(VAL_DPT_1));
      }
      state_Last[i] = state[i];
    }
    else
    {
     // SERIAL_PORT.print("CH");
     // SERIAL_PORT.print(i);
     // SERIAL_PORT.println(": gesperrt");
    }
  }
}

void ProcessSendCyclical()
{
  if ((knx.paramInt(getPar(BIN_CHSendenZyklisch, CH)) > 0) && (delayCheck(sendCyclicalDelay[CH], knx.paramInt(getPar(BIN_CHSendenZyklisch, CH)) * 1000)))
  {
    knx.getGroupObject(getCom(BIN_KoGO_STATE_, CH)).value(state_KO[CH], getDPT(VAL_DPT_1));
    sendCyclicalDelay[CH] = millis();
  }
  CH++;
  if (CH >= MAX_NUMBER_OF_Channels)
  {
    CH = 0;
  }
}

void ProcessInputs()
{
  if (delayCheck(ReadInputDelay, knx.paramByte(BIN_InputReadTime) * 100))
  {
    isReady = true;
    if (channel >= MAX_NUMBER_OF_Channels + 1)
    {
      channel = 0;
      CHState = SetVCC;
    }
    ReadInputDelay = millis();
  }

  if (isReady)
  {
    if (channel < MAX_NUMBER_OF_Channels)
    {
      //Statemaschine Read Inputs
      switch (CHState)
      {
      case SetVCC:
#ifdef Binaer_16CH
        pcf8575_VCC.pcf8575_WriteALL(state_VCC[channel]);
#endif
        CHState = ReadIO;
        break;

      case ReadIO:
        state[channel] = pcf8575_IO.pcf8575_Read_NEU(channel);
        CHState = ResetVCC;
        break;

      case ResetVCC:
#ifdef Binaer_16CH
        pcf8575_VCC.pcf8575_Write(state_VCC[channel], HIGH);
#endif
        CHState = SendKO;
        break;

      case SendKO:
        isChange(channel);
        CHState = SetLEDS;
        break;

      case SetLEDS:
        set_State_LED(channel);
        // Parameter1: Status LED "ON" / "OFF"  ||   Parameter2: Status LEDs KO gesteuert
        if (knx.paramByte(BIN_LEDdisplay) && knx.paramByte(BIN_LEDdisplayKOActivation) == 0 || status_Led_ON)
        {
          pcf8575_LED.pcf8575_WriteALL(state_LED_Out);
        }
        CHState = Finish;
        break;

      case Finish:
        channel++;
        if (channel == MAX_NUMBER_OF_Channels)
        {
          channel = MAX_NUMBER_OF_Channels + 1;
          isReady = false;
        }
        CHState = SetVCC;
        break;

      default:
        break;
      }
    }
  }
}
