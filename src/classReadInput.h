#pragma once

#include "hardware.h"
#include <knx.h>	
#include <stdint.h>
#include "BinaereingangDevice.h"

#include "PCF8575.h"

class classReadInput : public PCF8575
{
private:
    bool state;
    bool sperrobject;
    bool stateLED;

public:
    classReadInput();
    ~classReadInput();

    bool getState();
    bool getSperrobject();
    bool getStateLED();

    void setCH(uint8_t channel, uint8_t function);
    void readCH(uint8_t channel, uint8_t function);
    void sendCH(uint8_t channel, uint8_t function);
    void resetCH(uint8_t channel, uint8_t function);
    void setLEDS(uint8_t channel, uint8_t function);
};


