#include <Arduino.h>

#include <knx.h>
#include "hardware.h"
#include "KnxHelper.h"
#include "Binaereingang8CH.h"
#include "BinaereingangDevice.h"
#include "BinaereingangHandleCH.h"

uint32_t heartbeatDelay = 0;
uint32_t startupDelay = 0;

uint32_t TestDelay = 0;
bool TestLEDstate = false;

// true solgange der Start des gesamten Moduls verzögert werden soll
bool startupDelayfunc()
{
  if (knx.paramWord(BIN_StartupDelaySelection) == 0) // manuelle Eingabe
  {
    return !delayCheck(startupDelay, knx.paramInt(BIN_StartupDelay) * 1000);
  }
  else
  {
    return !delayCheck(startupDelay, knx.paramWord(BIN_StartupDelaySelection) * 1000);
  }
}

void ProcessHeartbeat()
{
  // the first heartbeat is send directly after startup delay of the device
  if (heartbeatDelay == 0 || delayCheck(heartbeatDelay, knx.paramInt(BIN_Heartbeat) * 1000))
  {
    // we waited enough, let's send a heartbeat signal
    knx.getGroupObject(BIN_KoHeartbeat).value(true, getDPT(VAL_DPT_1));

    heartbeatDelay = millis();
  }
}

void ProcessKoCallback(GroupObject &iKo)
{
  // Status LEDs ON/OFF
  if (iKo.asap() == BIN_KoStatusLedKO)
  {
    if ((bool)iKo.value(getDPT(VAL_DPT_1)) == true)
    {
      setLED_ON();
    }
    else
    {
      setLED_OFF();
    }
  }
  for (int koIndex = 0; koIndex < MAX_NUMBER_OF_Channels; koIndex++)
  {
    // Sperrobjekt
    if (iKo.asap() == BIN_KoOffset + (BIN_KoGO_SPERR_ + (koIndex * BIN_KoBlockSize)))
    {
      SERIAL_PORT.print("reviev KO: ");
      SERIAL_PORT.println(koIndex);
      SERIAL_PORT.println((bool)iKo.value(getDPT(VAL_DPT_1)));

      if ((bool)iKo.value(getDPT(VAL_DPT_1)) == 1)
      {
        set_state_Lock(koIndex);
      }
      else
      {
        reset_state_Lock(koIndex);
      }
    }
  }
}

void appSetup()
{
  // Disable 5V, to reset the I2C IO-Expander
  savePower();

  if (GroupObject::classCallback() == 0)
    GroupObject::classCallback(ProcessKoCallback);

  //delay to reset Pwer Supply
  delay(1000);

  // enable 5V again
  restorePower();

  //I2C Init
  Wire.begin();

  // load ETS parameters
  load_ETS_par();

  //Inputs Init
  initInputs();

  //State Maschine init
  CHState = SetVCC;
}

void appLoop()
{
  if (!knx.configured())
    return;

  // handle KNX stuff
  if (startupDelayfunc())
    return;
  
  ProcessInputs();

  ProcessSendCyclical();

  if (delayCheck(TestDelay, 500))
  {
    TestLEDstate = !TestLEDstate;
    //digitalWrite(LED_YELLOW_PIN, TestLEDstate);
    TestDelay = millis();
  }
}